# HruCase Docker Environment

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

Environment.

  - Docker version 19.03.3
  - docker-compose version 1.25.0

To run project...

```sh
$ cp .env.example .env
$ cp /php/supervisord.conf.example /php/supervisord.conf
$ submodule /web/www/hruc_admin (git@bitbucket:duck-duck/hruc_admin.git)
$ submodule /web/www/hruc_api (git@bitbucket:duck-duck/hruc_api.git)
$ docker-compose build
$ docker-compose up -d
$ docker-compose exec php bash
```
