#!/usr/bin/env sh
echo "################################## Run nginx"

envsubst '${ADMIN_DOMAIN} ${ADMIN_DOMAIN_PORT}' < /etc/nginx/conf.d/hruc_admin.conf.example > /etc/nginx/conf.d/hruc_admin.conf
envsubst '${API_DOMAIN} ${API_DOMAIN_PORT}' < /etc/nginx/conf.d/hruc_api.conf.example > /etc/nginx/conf.d/hruc_api.conf
nginx -g "daemon off;"
